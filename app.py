import os
import RPi.GPIO as GPIO
import tkinter
import time
import subprocess

Trig = 23          # Entree Trig du HC-SR04 branchee au GPIO 23
Echo = 24         # Sortie Echo du HC-SR04 branchee au GPIO 24
root = tkinter.Tk()
vid_ = False

def GPIO_init(Trig, Echo):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(Trig,GPIO.OUT)
    GPIO.setup(Echo,GPIO.IN)
    GPIO.output(Trig, False)

def dist(Trig, Echo, repet=1):
    print("dist appel")
    for x in range(repet):    # On prend la mesure "repet" fois
        time.sleep(1)       # On la prend toute les 1 seconde
        GPIO.output(Trig, True)
        time.sleep(0.00001)
        GPIO.output(Trig, False)
        while GPIO.input(Echo)==0:  ## Emission de l'ultrason
            debutImpulsion = time.time()
        while GPIO.input(Echo)==1:   ## Retour de l'Echo
            finImpulsion = time.time()
        distance = round((finImpulsion - debutImpulsion) * 340 * 100 / 2, 1)  ## Vitesse du son = 340 m/s
    return distance

GPIO_init(Trig, Echo)
val_init = dist(Trig, Echo)

def gap(value, valueref, inter=1):
    if value < (valueref + inter) and value > (valueref - inter):
        return True
    else:
        return False

def dist_compare(val, val_init):
    print("dist_compare")
    print(str(val))
    print(str(val_init))
    if gap(val, val_init, 0.5):
        return False
    else:
        val_init = val
        return True

def space(evt=None,  _root=root):
    if _vid == False:
        vid("/home/pi/velours_toujours/1.mov", _root)

def dist_v(evt=None, _Trig=Trig, _Echo=Echo, _root=root):
    dist_ = dist(_Trig, _Echo)
    if dist_compare(dist_, val_init):
        val_init == dist_
        if _vid == False:
            vid("/home/pi/velours_toujours/2.mov", _root)
    if vid_ == False:
        root.after(1000, dist_v)

def getLength(filename):
    result = subprocess.Popen(["ffprobe", filename],
        stdout = subprocess.PIPE, stderr = subprocess.STDOUT)
    return [x for x in result.stdout.readlines() if "Duration" in x]

def vid(path, root):
    vid_ = True
    os.system("omxplayer -b --no-osd --layer 2 "+path)
    vid_ = False

root.attributes("-fullscreen", 1)
root.title("Velvet Underground")
can=tkinter.Canvas(root,bg='white')
photo = tkinter.PhotoImage(file="/home/pi/velours_toujours/img.png")
can.create_image(0,0,image=photo,anchor=tkinter.NW)
can.config(height=photo.height(),width=photo.width())
can.pack()
root.bind("<space>", space)
root.after(1000, dist_v)
root.mainloop()