default_target: install

install:
	sudo apt-get install xterm -y
	mkdir /home/pi/.config/autostart
	sudo cp velours_toujours.desktop /home/pi/.config/autostart/velours_toujours.desktop